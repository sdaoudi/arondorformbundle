<?php

namespace Arondor\FormBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\Repository\Values\ContentType\ContentType;
use eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition;

use Arondor\FormBundle\Form\Form;
use Arondor\FormBundle\Helper\FormFieldType;

class FormController extends Controller
{
    /**
     * Returns an array of form fields based on the given content type
     * @return an array
     */
    protected function getFormFieldsFromContentType( ContentType $contentType )
    {
        // Get info collectors attributes
        $infoCollectors = $this->getInfoCollectorFields( $contentType );

        // Build the form fields based on fieldDefinitions
        $formFields = array();
        foreach ( $infoCollectors as $infoCollector )
        {
            $formFields[] = $this->buildFormField( $infoCollector );
        }
        return $formFields;
    }

    /**
     * Create a form field from the given fieldDefiniton
     * @return an array containing field description
     */
    protected function buildFormField( FieldDefinition $fieldDefinition )
    {
        // Helper to map the fieldTypes
        $fieldMappingHelper = $this->get( 'fieldMappingHelper' );
        // Get the default language code (may be diffrent from current?)
        $defaultLanguageCode = $this->getRepository()->getContentLanguageService()->getDefaultLanguageCode();

        $formField = new FormFieldType( $fieldDefinition, $fieldMappingHelper, $defaultLanguageCode );
        return $formField->getField();
    }

    /**
     * Get the fields which are marked as information collector in the given contentType
     * Returns an empty array if no information collector found
     *
     * @return eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition[]
     */
    protected function getInfoCollectorFields( ContentType $contentType )
    {
        // Get only the info collectors
        $infoCollectors = array();
        if ($contentType != null)
        {
            foreach ( $contentType->getFieldDefinitions() as $contentField )
            {
                if ( $contentField->isInfoCollector )
                {
                    $infoCollectors[] = $contentField;
                }
            }
        }
        return $infoCollectors;
    }
}
