# **Arondor Form Bundle** #

Arondor Form Bundle allows you to use legacy-style information collector to build eZ Publish 5 forms.

## Installation ##

The bundle is available on [packagist.org](https://packagist.org/packages/arondor/form-bundle).
So the easiest way is to use composer. To add the dependency, run the command :

```
#!shell

$ php composer.phar require arondor/form-bundle '@dev'
```

Then you must enable the bundle by updating ezpublish/EzPublishKernel.php as bellow :

```
#!php

<?php
// ezpublish/EzPublishKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Arondor\FormBundle\ArondorFormBundle(),
    );
}
```

## Usage ##

To use the Arondor Form Bundle you must have a content type that contains some information collector fields. For example you can use the default feedback_form.

Let's say you have a bundle named : MyTestBundle.

First you have to create an override rule.
The rule should be like the following :

```
#!yml

ezpublish:
    system:
        default:
            location_view:
                full:
                    feedback_form:
                        controller: MyTestBundle:Form:viewFormLocation
                        template: MyTestBundle:full:feedback_form.html.twig
                        match:
                            Identifier\ContentType: feedback_form
```

Then you have to create the custom controller. Here is an emxample of what it might look like :


```
#!php
<?php

namespace My\TestBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Arondor\FormBundle\Form\Form;

class FormController extends Controller
{
    public function viewFormLocationAction( $locationId, $viewType, $layout = false, array $params = array() )
    {
        $repository = $this->getRepository();
        // Get the content object type from the locationId
        $location = $repository->getLocationService()->loadLocation( $locationId );
        $content = $repository->getContentService()->loadContent( $location->contentInfo->id );
        $contentType = $repository->getContentTypeService()->loadContentType( $content->contentInfo->contentTypeId );

        // Get the default language code
        // Or you can use the code you want
        $defaultLanguageCode = $this->getRepository()->getContentLanguageService()->getDefaultLanguageCode();

        // Get the ArondorFormBundle service
        $formHelper = $this->get('formHelper');
        
        // Call the service to create the form fields
        // The returned array contains the fields described the Symfony2 way.
        // More info : http://symfony.com/fr/doc/current/reference/forms/types/form.html
        $formFields = $formHelper->getFormFieldsFromContentType($contentType, $defaultLanguageCode);
        
        // Create the form using ArondorFormBundle Form class
        $form = $this->createForm( new Form(), array( 'formFields' => $formFields ) );

        $params += array( 'form' => $form->createView() );

        $request = $this->getRequest();
        $form->handleRequest($request);

        if ( $form->isValid() )
        {
            // Get the posted data
            $data = $form->getData();
            // Do what you want with the data
            // ...
        }

        // Forward the request to the original ViewController
        // And get the response. Eventually alter it (here we change the smax-age for cache).
        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );
        $response->setSharedMaxAge( 600 );
        
        return $response;
    }
}

```

Finally, you need to create a view to display the form. Here is an example of a very basic view where we extends the demo pagelayout :


```
#!twig

{% extends "eZDemoBundle::pagelayout.html.twig" %}

{% block content %}
<section class="content-view-full">
    <div class="class-feedbackform row">
        {{ form(form) }}
    </div>
</section>
{% endblock %}
```

That's it! Your form should be visible and you can submit it to your custom controller.

## Form validation ##

Validation constraints are created automatically based on the information available in the content type. For example, lets say you have a content type with an attribute ezinteger as information collector. If you set it to :

* required
* limited to values between 0 and 100

the form will be created with a field integer, and the following constraints :

* NotBlank()
* Type('integer')
* Range(array('min' => 0, 'max' => 100))

Of course, the constraints depends on the field types and attributes.

Then to check if the form is valid when submitted, you just have to use :

```
#!php
<?php

$form->handleRequest($request);

// Check validation constraints
if ( $form->isValid() )
{
    // Use your valid data here
}

```

## Use the legacy information collection ##

The formHelper service offers you the possibility to send your data to the legacy information collection so the posted data are visible directly from the back office.

To do so, juste call the following method when your data is posted :


```
#!php
<?php

// Get the posted data
$data = $form->getData();
// Use the legacy collector information
$formHelper->legacyInformationCollection( $this->getLegacyKernel(), $content->id, $data );

```

But be aware that this method is not fully identical to the legacy one. There is still some work to be done. For example, email is not send.


## License ##

The ArondorFormBundle is under the MIT license. You can read the full license at :

```
#!yml

Resources/meta/LICENSE
```

## Known issues ##

* You shouldn't be using checkboxes. They are not supported yet.
* Legacy information collection functionnality needs to be enhanced.