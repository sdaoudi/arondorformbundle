<?php

namespace Arondor\FormBundle\Form;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Class used to build the email sharing form
 * 
 * @author julien montavit
 *
 */
class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formFields = $options['data']['formFields'];
        foreach ( $formFields as $formField )
        {
            $builder->add( $formField['name'], $formField['type'], $formField['params'] );
        }
        $builder->setMethod('POST');
        $builder->add('Send', 'submit');
    }
    
    public function getName()
    {
        return 'form';
    }
}


?>