<?php

namespace Arondor\FormBundle\Tests\Helper;

use Arondor\FormBundle\Helper\FormHelper;
use PHPUnit_Framework_TestCase;
use eZ\Bundle\EzPublishCoreBundle\Kernel;
use eZ\Publish\Core\FieldType\TextLine\Value;


/**
 * Class used to test the FormHelper
 */
class FormHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * Init class variables here
     */
    protected function setUp()
    {
    	parent::setUp();
    }

    /**
     * Test getInfoCollectorFields
     */
    public function testGetInfoCollectorFields()
    {
        $fieldDef1 = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef1
            ->expects($this->any())
            ->method('__get')
            ->will($this->returnValue(true));

        $fieldDef2 = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef2
            ->expects($this->any())
            ->method('__get')
            ->will($this->returnValue(false));


        $contentType = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\ContentType' )
            ->setConstructorArgs( array( array( 'fieldDefinitions' => array($fieldDef1, $fieldDef2) ) ) )
            ->getMock();
        $contentType
            ->expects($this->once())
            ->method('getFieldDefinitions')
            ->will($this->returnValue(array($fieldDef1, $fieldDef2)));


        $helper = new FormHelper();
        $result = $helper->getInfoCollectorFields( $contentType );

        foreach ($result as $infoCollector)
        {
            $this->assertTrue($infoCollector->isInfoCollector);
        }
    }

    /**
     * Test getInfoCollectorFields returning no result
     */
    public function testGetInfoCollectorFieldsEmpty()
    {
        $fieldDef = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef
            ->expects($this->any())
            ->method('__get')
            ->will($this->returnValue(false));

        $contentType = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\ContentType' )
            ->setConstructorArgs( array( array( 'fieldDefinitions' => array($fieldDef) ) ) )
            ->getMock();
        $contentType
            ->expects($this->once())
            ->method('getFieldDefinitions')
            ->will($this->returnValue(array($fieldDef)));

        $helper = new FormHelper();
        $emptyResult = $helper->getInfoCollectorFields( $contentType );
        $this->assertEmpty($emptyResult);
    }

    /**
     * Test buildFormField
     */
    public function testBuildFormField()
    {
        $fieldDef = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef
            ->expects($this->any())
            ->method('__get')
            ->will($this->returnValue(''));

        $expected = array(
            'name' => '',
            'type' => null,
            'params' => array(
                'required' => '',
                'label' => null,
                'data' => '',
                'empty_data' => '',
                'label_attr' => array(),
                'read_only' => null,
                'disabled' => null,
                'attr' => array()
                )
            );

        $helper = new FormHelper();
        $result = $helper->buildFormField( $fieldDef, 'fre-FR' );

        $this->assertEquals($result, $expected);
    }

    /**
     * Test getFormFieldsFromContentType
     */
    public function testGetFormFieldsFromContentType()
    {
        $fieldValue = new Value('Default value');
        $fieldDef1 = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef1
            ->expects($this->at(0))
            ->method('__get')
            ->with('isInfoCollector')
            ->will($this->returnValue(true));
        $fieldDef1
            ->expects($this->at(1))
            ->method('__get')
            ->with('identifier')
            ->will($this->returnValue('identifier'));
        $fieldDef1
            ->expects($this->at(2))
            ->method('__get')
            ->with('fieldTypeIdentifier')
            ->will($this->returnValue('ezstring'));
        $fieldDef1
            ->expects($this->at(3))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDef1
            ->expects($this->at(4))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDef1
            ->expects($this->at(5))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDef1
            ->expects($this->at(6))
            ->method('__get')
            ->with('isRequired')
            ->will($this->returnValue(true));
        $fieldDef1
            ->expects($this->once())
            ->method('getName')
            ->with('fre-FR')
            ->will($this->returnValue('Field nice name'));

        $fieldDef2 = $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition');
        $fieldDef2
            ->expects($this->once())
            ->method('__get')
            ->with('isInfoCollector')
            ->will($this->returnValue(false));

        $contentType = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\ContentType' )
            ->disableOriginalConstructor()
            ->getMock();
        $contentType
            ->expects($this->once())
            ->method('getFieldDefinitions')
            ->will($this->returnValue(array($fieldDef1, $fieldDef2)));

        $expected = array(
            array(
                'name' => "identifier",
                'type' => 'text',
                'params' => array(
                    'required' => true,
                    'label' => "Field nice name",
                    'data' => $fieldValue->text,
                    'empty_data' => "",
                    'label_attr' => array(),
                    'read_only' => NULL,
                    'disabled' => NULL,
                    'attr' => array()
                    )
                )
            );

        $helper = new FormHelper();
        $result = $helper->getFormFieldsFromContentType( $contentType, 'fre-FR' );
        $this->assertEquals($expected, $result);
    }
}
