<?php

namespace Arondor\FormBundle\Tests\Helper;

use Arondor\FormBundle\Helper\FieldMappingHelper;
use PHPUnit_Framework_TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use eZ\Bundle\EzPublishCoreBundle\Kernel;


/**
 * Class used to test the FieldMappingHelper 
 */
class FieldMappingHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * Init class variables here
     */
    protected function setUp()
    {
    	parent::setUp();
    }

    public function testGetSymfonyType()
    {
        // Array of types to test
        $tests = array(
            'ezstring' => 'text',
            'eztext' => 'textarea',
            'ezselection' => 'choice',
            'ezemail' => 'email',
            'ezcountry' => 'country',
            'ezboolean' => 'choice'
            );

        $helper = new FieldMappingHelper();
        foreach ($tests as $test => $expected) {
            $this->assertEquals( $helper->getSymfonyType( $test ), $expected );
        }
        $this->assertEquals( $helper->getSymfonyType( 'wrong' ), null );
    }

}
