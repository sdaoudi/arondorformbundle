<?php

namespace Arondor\FormBundle\Tests\Helper;

use Arondor\FormBundle\Helper\FormFieldType;
use PHPUnit_Framework_TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use eZ\Bundle\EzPublishCoreBundle\Kernel;
use eZ\Publish\Core\FieldType\TextLine\Value;


/**
 * Class used to test the FieldMappingHelper 
 */
class FormFieldTypeTest extends PHPUnit_Framework_TestCase
{

    public $fieldDefName = 'testFieldDef';
    public $fieldDefTypeEz = 'ezstring';
    public $fieldDefTypeSf = 'text';
    public $isRequired = true;
    public $fieldDefDefaultValue = 'Default value';

    /**
     * Init class variables here
     */
    protected function setUp()
    {
    	parent::setUp();

        // Init field definition test values
        $this->fieldDefIdentifier = 'testFieldDef';
        $this->fieldDefName = 'Field def nice name';
        $this->fieldDefTypeEz = 'ezstring';
        $this->fieldDefTypeSf = 'text';
        $this->isRequired = true;
        $this->fieldDefDefaultValue = 'Default value';
    }

    /**
     * Checks that the constructor correctly sets the class public variables 
     */
    public function testConstructorPublicProperties()
    {
        $fieldMappingHelper = $this->getMock('Arondor\FormBundle\Helper\FieldMappingHelper');
        $fieldMappingHelper
            ->expects($this->at(0))
            ->method('getSymfonyType')
            ->with($this->fieldDefTypeEz)
            ->will($this->returnValue($this->fieldDefTypeSf));

        $fieldValue = new Value($this->fieldDefDefaultValue);
        $fieldDefinition = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition' )
            ->setConstructorArgs( array( array( 'defaultValue' => $fieldValue ) ) )
            ->getMock();
        $fieldDefinition
            ->expects($this->at(0))
            ->method('__get')
            ->with('identifier')
            ->will($this->returnValue($this->fieldDefName));
        $fieldDefinition
            ->expects($this->at(1))
            ->method('__get')
            ->with('fieldTypeIdentifier')
            ->will($this->returnValue($this->fieldDefTypeEz));
        $fieldDefinition
            ->expects($this->at(2))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(3))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(4))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(5))
            ->method('__get')
            ->with('isRequired')
            ->will($this->returnValue($this->isRequired));
        $fieldDefinition
            ->expects($this->once())
            ->method('getName')
            ->with('fre-FR')
            ->will($this->returnValue($this->fieldDefName));
        
        $fieldType = new FormFieldType($fieldDefinition, $fieldMappingHelper, 'fre-FR');
        $this->assertEquals( $this->fieldDefName, $fieldType->name );
        $this->assertEquals( $this->fieldDefTypeSf, $fieldType->type );
    }

    public function testGetFieldDefDefaultValue()
    {
        $formFieldType = new FormFieldType(
            $this->getMock('eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition'), 
            $this->getMock('Arondor\FormBundle\Helper\FieldMappingHelper'), 
            'fre-FR'
            );

        $fieldValue = $this->getMock('eZ\Publish\Core\FieldType\TextLine\Value');
        $fieldValue
            ->expects($this->any())
            ->method('__get')
            ->with('text')
            ->will($this->returnValue($this->fieldDefDefaultValue));

        $fieldDef = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition' )
            ->setConstructorArgs( array( array( 'defaultValue' => $fieldValue ) ) )
            ->getMock();

        $fieldDef
            ->expects($this->any())
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));

        // Check the output
        $this->assertEquals( $fieldValue->text, $formFieldType->getFieldDefDefaultValue($fieldDef) );
    }

    public function testGetParams()
    {
        $fieldMappingHelper = $this->getMock('Arondor\FormBundle\Helper\FieldMappingHelper');
        $fieldMappingHelper
            ->expects($this->once())
            ->method('getSymfonyType')
            ->with($this->fieldDefTypeEz)
            ->will($this->returnValue($this->fieldDefTypeSf));

        $fieldValue = new Value($this->fieldDefDefaultValue);
        $fieldDefinition = $this
            ->getMockBuilder( 'eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition' )
            ->setConstructorArgs( array( array( 'defaultValue' => $fieldValue ) ) )
            ->getMock();
        $fieldDefinition
            ->expects($this->at(0))
            ->method('__get')
            ->with('identifier')
            ->will($this->returnValue($this->fieldDefIdentifier));
        $fieldDefinition
            ->expects($this->at(1))
            ->method('__get')
            ->with('fieldTypeIdentifier')
            ->will($this->returnValue($this->fieldDefTypeEz));
        $fieldDefinition
            ->expects($this->at(2))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(3))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(4))
            ->method('__get')
            ->with('defaultValue')
            ->will($this->returnValue($fieldValue));
        $fieldDefinition
            ->expects($this->at(5))
            ->method('__get')
            ->with('isRequired')
            ->will($this->returnValue($this->isRequired));
        $fieldDefinition
            ->expects($this->once())
            ->method('getName')
            ->with('fre-FR')
            ->will($this->returnValue($this->fieldDefName));

        $expected = array(
            'required' => $this->isRequired,
            'label' => $this->fieldDefName,
            'data' => $this->fieldDefDefaultValue,
            'empty_data' => '',
            'label_attr' =>  array(),
            'read_only' => null,
            'disabled' => null,
            'attr' => array()
            );
        
        $fieldType = new FormFieldType($fieldDefinition, $fieldMappingHelper, 'fre-FR');

        $class = new \ReflectionClass('Arondor\FormBundle\Helper\FormFieldType');
        $method = $class->getMethod('getParams');
        $method->setAccessible(true);

        $result = $method->invokeArgs($fieldType, array());
        // Checking output
        $this->assertEquals($expected, $result);
    }
}
