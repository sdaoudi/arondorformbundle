<?php

namespace Arondor\FormBundle\Helper;

use eZ\Publish\Core\Repository\Values\ContentType\ContentType;
use eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition;

use Arondor\FormBundle\Helper\FormFieldType;
use Arondor\FormBundle\Helper\FieldMappingHelper;

class FormHelper
{
    /**
     * Returns an array of form fields based on the given content type
     * @return an array
     */
    public function getFormFieldsFromContentType( ContentType $contentType, $defaultLanguageCode )
    {
        // Get info collectors attributes
        $infoCollectors = $this->getInfoCollectorFields( $contentType );

        // Build the form fields based on fieldDefinitions
        $formFields = array();
        foreach ( $infoCollectors as $infoCollector )
        {
            $formFields[] = $this->buildFormField( $infoCollector, $defaultLanguageCode );
        }
        return $formFields;
    }

    /**
     * Create a form field from the given fieldDefiniton
     * @return an array containing field description
     */
    public function buildFormField( FieldDefinition $fieldDefinition, $defaultLanguageCode )
    {
        // Helper to map the fieldTypes
        $formField = new FormFieldType( $fieldDefinition, new FieldMappingHelper(), $defaultLanguageCode );
        return $formField->getField();
    }

    /**
     * Get the fields which are marked as information collector in the given contentType
     * Returns an empty array if no information collector found
     *
     * @return eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition[]
     */
    public function getInfoCollectorFields( ContentType $contentType )
    {
        // Get only the info collectors
        $infoCollectors = array();
        if ($contentType != null)
        {
            foreach ( $contentType->getFieldDefinitions() as $contentField )
            {
                if ( $contentField->isInfoCollector )
                {
                    $infoCollectors[] = $contentField;
                }
            }
        }
        return $infoCollectors;
    }

    /**
     * Calls the legacy information collector to store the data in the database
     * The information are visible from the back office, just like 
     * the original information collector functionnality
     *
     */
    public function legacyInformationCollection( $legacyKernel, $contentId, array $formData )
    {
        return $legacyKernel->runCallback(
            function () use ( $contentId, $formData )
            {
                if ( $contentId != null and !empty( $formData ) )
                {
                    // Create a new collection
                    $collection = \eZInformationCollection::create( $contentId, \eZInformationCollection::currentUserIdentifier() );
                    $collection->store();
                    $newCollection = true;

                    $object = \eZContentObject::fetch( $contentId );
                    $version = $object->currentVersion();
                    $contentObjectAttributes = $version->contentObjectAttributes();

                    $db = \eZDB::instance();
                    $db->begin();

                    foreach ( array_keys( $contentObjectAttributes ) as $key )
                    {
                        $contentObjectAttribute = $contentObjectAttributes[$key];
                        $contentClassAttribute = $contentObjectAttribute->contentClassAttribute();

                        if ( $contentClassAttribute->attribute( 'is_information_collector' ) )
                        {
                            // Collect the information for the current attribute
                            if ( $newCollection )
                                $collectionAttribute = \eZInformationCollectionAttribute::create( $collection->attribute( 'id' ) );
                            else
                                $collectionAttribute = \eZInformationCollectionAttribute::fetchByObjectAttributeID( $collection->attribute( 'id' ), $contentObjectAttribute->attribute( 'id' ) );
                            
                            $collectionAttribute->setAttribute( 'contentclass_attribute_id', $contentObjectAttribute->attribute( 'contentclassattribute_id' ) );
                            $collectionAttribute->setAttribute( 'contentobject_attribute_id', $contentObjectAttribute->attribute( 'id' ) );
                            $collectionAttribute->setAttribute( 'contentobject_id', $contentObjectAttribute->attribute( 'contentobject_id' ) );

                            // Check the datatype before
                            $collectionAttribute->setAttribute( 'data_text', $formData[$contentClassAttribute->attribute('identifier')] );

                            if ( $collectionAttribute )
                            {
                                $collectionAttribute->store();
                            }
                        }
                    }
                    $db->commit();
                    $collection->sync();
                }
            }
        );
    }
}
