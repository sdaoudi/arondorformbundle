<?php

namespace Arondor\FormBundle\Helper;

/**
 * Class used to map eZ field types to Symfony2 types 
 */
class FieldMappingHelper
{
    /**
     * Returns the symfony field type corresponding to the given eZ field type
     */
    public function getSymfonyType( $eZFieldType )
    {
        switch ($eZFieldType)
        {
            case 'ezstring':
                return 'text';
            case 'eztext':
                return 'textarea';
            case 'ezselection':
                return 'choice';
            case 'ezemail':
                return 'email';
            case 'ezcountry':
                return 'country';
            case 'ezboolean':
                return 'choice';
            case 'ezinteger':
                return 'integer';
            default:
                return null;
        }
    }
}