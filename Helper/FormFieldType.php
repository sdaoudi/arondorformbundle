<?php

namespace Arondor\FormBundle\Helper;

use eZ\Publish\Core\Repository\Values\ContentType\FieldDefinition;
use Arondor\FormBundle\Helper\FieldMappingHelper;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * 
 */
class FormFieldType
{

    public $name;
    public $type;

    // Commons
    protected $data;
    protected $emptyData = '';
    protected $required;
    protected $label;
    protected $labelAttr = array();
    protected $readOnly;
    protected $disabled;
    protected $attr = array();
    protected $constraints = array();

    // Choice type
    protected $choices = array();
    protected $multiple = false;
    protected $expanded = false;

    /**
     * Constructor of the class
     * It fills the minimum data needed to create a form field
     */
    public function __construct( FieldDefinition $fieldDefinition, FieldMappingHelper $fieldMappingHelper, $languageCode )
    {
        $this->name = $fieldDefinition->identifier;
        $this->type = $fieldMappingHelper->getSymfonyType( $fieldDefinition->fieldTypeIdentifier );
        
        $this->data = $this->getFieldDefDefaultValue( $fieldDefinition );
        $this->required = $fieldDefinition->isRequired;
        $this->label = $fieldDefinition->getName( $languageCode );

        if ( $this->type == 'choice' )
        {
            // Choice list
            $fieldSettings = $fieldDefinition->fieldSettings;
            if ( !empty( $fieldSettings ) )
            {
                if ( array_key_exists( 'options', $fieldSettings ) )
                {
                    $this->choices = $fieldSettings['options'];
                }

                if ( array_key_exists( 'isMultiple', $fieldSettings ) )
                {
                    $this->multiple = $fieldSettings['isMultiple'];
                }
            } 
            else 
            {
                // Checkbox
                // eZ legacy checkboxes are very different from Symfonys
                // In eZ there is no value assinged, so we use the identifier
                // You must take this into account during rendering and processing form
                $this->choices = array( $this->name );
                $this->multiple = true;
                $this->expanded = true;
                $this->emptyData = array();
                $this->data = array();
            }
        }

        // Setting up validation constraints
        $this->constraints = $this->setConstraints($fieldDefinition);
    }

    /**
     * Return the field data
     * Function used to pass data to the Symfony form builder to add a new field
     *
     * @return array
     */
    public function getField()
    {
        return array(
            'name' => $this->name, 
            'type' => $this->type, 
            'params' => $this->getParams() 
            );
    }

    /**
     * Returns the default value of the given field definition
     *
     * @return mixed
     */
    public function getFieldDefDefaultValue( FieldDefinition $fieldDefinition )
    {
        $value = '';
        if ( property_exists( $fieldDefinition, 'defaultValue' ) && $fieldDefinition->defaultValue != null )
        {
            if ( property_exists( $fieldDefinition->defaultValue, 'text' ) )
            {
                $value = $fieldDefinition->defaultValue->text;
            } 
            else if ( property_exists( $fieldDefinition->defaultValue, 'bool' ) )
            {
                $value = $fieldDefinition->defaultValue->bool;
            }
        }
        return $value;
    }

    /**
     * Returns an array of constraints based on field definition
     */
    protected function setConstraints( FieldDefinition $fieldDefinition )
    {
        $constraints = array();

        if ( $this->type == 'integer' )
        {
            $constraints[] = new Type('integer');
            $range = $fieldDefinition->validatorConfiguration['IntegerValueValidator'];
            $constraints[] = new Range(array('min' => $range['minIntegerValue'], 'max' => $range['maxIntegerValue']));
            $this->data = 0; // Change default value...
        } 
        else if ( $this->type == 'email' ) 
        {
            $constraints[] = new Email();
        } 
        else if ( $this->type == 'text' ) 
        {
            $maxLen = $fieldDefinition->validatorConfiguration['StringLengthValidator']['maxStringLength'];
            $constraints[] = new Length(0, $maxLen);
        }

        if ( $this->required === true ) 
        {
            $constraints[] = new NotBlank();
        }

        return $constraints;
    }

    /**
     * Puts the field params in an array with keys that Symfony uses to build a form
     *
     * @return array
     */
    protected function getParams()
    {
        $params = array();

        $params['required'] = $this->required;
        $params['label'] = $this->label;
        $params['data'] = $this->data;
        $params['empty_data'] = $this->emptyData;
        $params['label_attr'] = $this->labelAttr;
        $params['read_only'] = $this->readOnly;
        $params['disabled'] = $this->disabled;
        $params['attr'] = $this->attr;
        $params['constraints'] = $this->constraints;

        if ( $this->type == 'choice' )
        {
            $params['choices'] = $this->choices;
            $params['multiple'] = $this->multiple;
            $params['expanded'] = $this->expanded;
            $params['empty_value'] = true;
        }

        return $params;
    }
}